import { combineReducers, configureStore } from '@reduxjs/toolkit';
import commentsReducer from './Comments/reducer';
import commentInfoReducer from './CommentInfo/reducer';

const rootReducers = combineReducers({
	comments: commentsReducer,
	commentInfo: commentInfoReducer
})

export const store = configureStore({reducer: rootReducers});