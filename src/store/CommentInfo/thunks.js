import { commentsApi } from '../../api/commentsApi';
import { setCommentInfoError, setCommentInfoLoading, setCommentInfoSuccess } from './actions';

export const setCommentInfo = (commentId) => (dispatch) => {
	commentsApi
		.getCommentInfo(commentId)
		.then(commentInfo => {
			dispatch(setCommentInfoLoading(true));

			dispatch(setCommentInfoSuccess(commentInfo));

			dispatch(setCommentInfoLoading(false));
		})
		.catch(error => {
			dispatch(setCommentInfoLoading(false));
			dispatch(setCommentInfoError(error.message));
		});
}