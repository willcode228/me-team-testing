import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CommentsItem from './CommentsItem/CommentsItem';
import Error from '../../../common/Error/Error';
import CommentsPagination from './CommentsPagination/CommentsPagination';
import styles from './CommentsBody.module.scss'
import { setComments } from '../../../store/Comments/thunks';

const CommentsBody = () => {
	const dispatch = useDispatch();
	const {result, error, errorMessage, isSearched } = useSelector(state => state.comments);

	useEffect(() => {
		const startParams = {
			searchQuery: '',
			isSorted: false,
			page: 1
		}
		dispatch(setComments(startParams));
	}, []);

	return (
		<div className={styles.body}>

			<ul>
				{result.map(comment => <CommentsItem key={comment.id} comment={comment}/>)}
			</ul>

			<Error errorInfo={{errorMessage, error}}/>

			{isSearched && !result.length ?
				<h3 className={styles.title}>Sorry we didn't find anything</h3> : null}

			{isSearched
				? <CommentsPagination />
				: <h3 className={styles.title}>Type something and all results will be here</h3>}

		</div>
	);
};

export default CommentsBody;