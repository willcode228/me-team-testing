import rootApi from './rootApi';


const getComments = ({searchQuery, page = 1, isSorted = false}) => {
	return rootApi
		.get(`?email_like=^${searchQuery}&_page=${page}&_sort=${isSorted && 'name'}`)
		.then(response => response.data);
}

const getCommentInfo = (commentId) => {
	return rootApi
		.get(`${commentId}`)
		.then(response => response.data);
}

export const commentsApi = {getComments, getCommentInfo};