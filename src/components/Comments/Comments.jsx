import React from 'react';
import CommentsSearch from './CommentsSearch/CommentsSearch';
import CommentsBody from './CommentsBody/CommentsBody';
import styles from './Comments.module.css';

const Comments = () => (
		<div className={styles.home}>
			<CommentsSearch />
			<CommentsBody />
		</div>
);

export default Comments;