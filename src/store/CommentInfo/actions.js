import { createAction } from '@reduxjs/toolkit';

export const setCommentInfoSuccess = createAction('COMMENT_INFO/SET_COMMENT_INFO_SUCCESS');
export const setCommentInfoError = createAction('COMMENT_INFO/SET_COMMENT_INFO_ERROR');
export const setCommentInfoLoading = createAction('COMMENT_INFO/SET_COMMENT_INFO_LOADING');