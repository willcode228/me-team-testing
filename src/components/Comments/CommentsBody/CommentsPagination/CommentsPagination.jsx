import React from 'react';
import { changeSearchPage } from '../../../../store/Comments/thunks';
import { useDispatch, useSelector } from 'react-redux';
import styles from './CommentsPagination.module.scss';

const CommentsPagination = () => {
	const dispatch = useDispatch();
	const {searchParams: {page}, result, isSearched} = useSelector(state => state.comments);

	return (
		<div className={styles.pagination}>
			<button
				disabled={page <= 1}
				onClick={() => dispatch(changeSearchPage('prev'))}
			>
				prev
			</button>

			<button
				disabled={!result.length && isSearched}
				onClick={() => dispatch(changeSearchPage('next'))}
			>
				next
			</button>
		</div>
	);
};

export default CommentsPagination;