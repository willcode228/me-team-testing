import {
	setCommentsError,
	setCommentsLoading,
	setCommentsSuccess,
	setFavouriteComments,
	setNewSearchPage,
	setSearchParams
} from './actions';
import { createReducer } from '@reduxjs/toolkit';

const initialState = {
	result: [],
	favourites: [],
	isSearched: false,
	searchParams: {},
	loading: false,
	error: false,
	errorMessage: '',
};

const commentsReducer = createReducer(initialState, {
	[setCommentsSuccess]: (state, action) => {
		state.result = action.payload;
	},
	[setCommentsLoading]: (state, action) => {
		state.loading = action.payload;
		state.isSearched = true;
	},
	[setCommentsError]: (state, action) => {
		state.error = true;
		state.errorMessage = action.payload;
	},

	[setSearchParams]: (state, action) => {
		state.searchParams = action.payload
	},
	[setNewSearchPage]: (state, action) => {
		state.searchParams.page = action.payload;
	},

	[setFavouriteComments]: (state, action) => {
			state.favourites = action.payload;
	}
});

export default commentsReducer;