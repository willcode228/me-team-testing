import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { setComments } from '../../../store/Comments/thunks';
import styles from './CommentsSearch.module.scss';
import { ReactComponent as SearchIcon } from '../../../assets/search.svg';

const CommentsSearch = () => {

	const dispatch = useDispatch();
	const [searchQuery, setSearchQuery] = useState('');
	const [isSorted, setIsSorted] = useState(false);

	const clearSearchInput = e => {
		e.preventDefault();
		setSearchQuery('');
	};

	const changeSearchInput = e => {
		setSearchQuery(e.target.value);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		if (searchQuery.trim()) {
			dispatch(setComments({searchQuery, isSorted, page: 1}));
		} else {
			setSearchQuery('Write the search text');
		}
	};

	return (
		<>
			<form onSubmit={handleSubmit}>

				<div className={`${styles.form} ${searchQuery && styles.active}`}>

					<label className={styles.form__label} htmlFor="search-input">Type to search</label>

					<input className={styles.form__input} id="search-input"
								 type="text" autoComplete="off"
								 value={searchQuery} onChange={changeSearchInput}/>

					{searchQuery &&
						<>
							<button
								type="button"
								onClick={clearSearchInput}
								className={`${styles.form__del} ${styles.form__btn}`}
							>
								✕
							</button>

							<button
								type="submit"
								className={`${styles.form__search} ${styles.form__btn}`}
							>
								<SearchIcon/>
							</button>
						</>}
				</div>


				<div className={styles.form__sort}>
					<input
						type="checkbox"
						id="sort-input"
						checked={isSorted}
						onChange={() => setIsSorted(a => !a)}
					/>

					<label htmlFor="sort-input">
						{isSorted ? 'Disable' : 'Enable'} sorting by name
					</label>
				</div>

			</form>
		</>
	);
};

export default CommentsSearch;