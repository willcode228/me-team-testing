import { createAction } from '@reduxjs/toolkit';

export const setCommentsSuccess = createAction('COMMENTS/SET_COMMENTS_SUCCESS');
export const setCommentsError = createAction('COMMENTS/SET_COMMENTS_ERROR');
export const setCommentsLoading = createAction('COMMENTS/SET_COMMENTS_LOADING');

export const setSearchParams = createAction('COMMENTS/SET_SEARCH_PARAMS');
export const setNewSearchPage = createAction('COMMENTS/SET_NEW_SEARCH_PAGE');

export const setFavouriteComments = createAction('COMMENTS/SET_FAVOURITE_COMMENTS');