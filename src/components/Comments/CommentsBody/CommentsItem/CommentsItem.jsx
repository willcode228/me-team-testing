import React from 'react';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { toggleFavourite } from '../../../../store/Comments/thunks';
import { COMMENT_INFO } from '../../../../routes/consts';
import styles from './CommentsItem.module.scss';

const CommentsItem = ({comment}) => {
	const dispatch = useDispatch();
	const {favourites} = useSelector(state => state.comments);

	const {id, email, name} = comment;
	const status = favourites.includes(id) ? 'Remove from favourite' : 'Toggle to favourite';

	const changeCommentStatus = () => {
		dispatch(toggleFavourite(id));
	}

	return (
		<li className={styles.comment}>
			<p>Email: {email}</p>
			<p>Name: {name}</p>

			<div className={styles.comment__btngroup}>
				<NavLink to={`${COMMENT_INFO}/${id}`}>View more</NavLink>
				<button onClick={changeCommentStatus}>{status}</button>
			</div>
		</li>
	);
};

export default CommentsItem;