import axios from 'axios';

const rootApi = axios.create({
	baseURL: 'https://jsonplaceholder.typicode.com/comments/',
	params: {
		'_limit': 5
	}
});

export default rootApi;