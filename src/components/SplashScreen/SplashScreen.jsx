import React from 'react';
import styles from'./SplashScreen.module.scss'

const SplashScreen = ({visible}) => (
	<div className={`${styles.screen} ${!visible && styles.invisible}`}>
		<h2>Danial</h2>
	</div>
);

export default SplashScreen;