import {Routes, Route, Navigate} from 'react-router';
import Comments from '../components/Comments/Comments';
import CommentInfo from '../components/CommentInfo/CommentInfo';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { setFavouriteComments } from '../store/Comments/actions';
import { COMMENT_INFO, HOME } from '../routes/consts';
import styles from './App.module.scss';
import SplashScreen from '../components/SplashScreen/SplashScreen';


const App = () => {
  const dispatch = useDispatch();
  const [splashScreenVisible, setSplashScreenVisible] = useState(true);

  useEffect(() => {
    if(localStorage.getItem('favourites')) {
      const favourites = JSON.parse(localStorage.getItem('favourites'));
      dispatch(setFavouriteComments(favourites));
    }

    const splashTimer = setTimeout(() => {
      setSplashScreenVisible(false);
    }, 3000);

    return () => { clearTimeout(splashTimer); };
  }, []);

  return (
    <div className={styles.app}>
      <SplashScreen visible={splashScreenVisible}/>
      <Routes>
        <Route path={HOME} element={<Comments/>} end/>
        <Route path={`${COMMENT_INFO}/:commentId`} element={<CommentInfo/>} end/>

        <Route path="/" element={<Navigate to={HOME}/>}/>
      </Routes>
    </div>
  );
};

export default App;
