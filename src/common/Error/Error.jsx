import React from 'react';

const Error = ({errorInfo}) => (
	<>{errorInfo.error &&
		<h3>Sorry it's some errors here: {errorInfo.errorMessage}</h3>
	}</>
);

export default Error;