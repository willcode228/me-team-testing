import { createReducer } from '@reduxjs/toolkit';
import { setCommentInfoError, setCommentInfoLoading, setCommentInfoSuccess } from './actions';

const initialState = {
	info: {},
	error: false,
	errorMessage: false,
	loading: false
}

const commentInfoReducer = createReducer(initialState, {
	[setCommentInfoSuccess]: (state, action) => {
		state.info = action.payload
	},
	[setCommentInfoError] : (state, action) => {
		state.error = true;
		state.errorMessage = action.payload;
	},
	[setCommentInfoLoading]: (state, action) => {
		state.loading = action.payload;
	}
});

export default commentInfoReducer;