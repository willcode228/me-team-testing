import React, { useEffect } from 'react';
import { setCommentInfo } from '../../store/CommentInfo/thunks';
import { useMatch } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { COMMENT_INFO, HOME } from '../../routes/consts';
import Error from '../../common/Error/Error';
import styles from './CommentInfo.module.scss';

const CommentInfo = () => {
	const dispatch = useDispatch();
	const {params: {commentId}} = useMatch(`${COMMENT_INFO}/:commentId`);
	const {info, error, errorMessage, loading} = useSelector(state => state.commentInfo);

	useEffect(() => {
		dispatch(setCommentInfo(commentId));
	}, [commentId]);

	return (
		<div className={styles.info}>
			<Error errorInfo={{errorMessage, error}}/>

			<p>Name: {info.name}</p>
			<p>Email: {info.email}</p>
			<p>Body: {info.body}</p>

			<NavLink to={HOME}>Turn Back</NavLink>
		</div>
	);
};

export default CommentInfo;