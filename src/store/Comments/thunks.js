import { commentsApi } from '../../api/commentsApi';
import {
	setCommentsError,
	setCommentsLoading,
	setCommentsSuccess,
	setFavouriteComments,
	setNewSearchPage,
	setSearchParams
} from './actions';

export const setComments = (searchParams) => (dispatch) => {
	commentsApi
		.getComments(searchParams)
		.then(comments => {
			dispatch(setCommentsLoading(true));

			dispatch(setSearchParams(searchParams));
			dispatch(setCommentsSuccess(comments));

			dispatch(setCommentsLoading(false));
		})
		.catch(error => {
			dispatch(setCommentsLoading(false));
			dispatch(setCommentsError(error.message));
		});
}

export const changeSearchPage = (direction) => (dispatch, getState) => {
	const searchParams = getState().comments.searchParams;
	let page = searchParams.page;

	direction === 'next' ? page++ : page--;

	dispatch(setComments({...searchParams, page}));
	dispatch(setNewSearchPage(page));
}

export const toggleFavourite = (id) => (dispatch, getState) => {
	let favourites = getState().comments.favourites;

	if(favourites.includes(id)){
		favourites = favourites.filter(favId => favId !== id);
	} else {
		favourites = [...favourites, id];
	}

	localStorage.setItem('favourites', JSON.stringify(favourites));
	dispatch(setFavouriteComments(favourites));
}